param(
    [string]$path
)
function GeneratePot{
    param ([string]$path)
    $currentLocation = Get-Location
    $appFolder = $currentLocation.Path + '\app'
    if(Test-Path $appFolder){
        node $path $appFolder
    }
    else {
        node $path $currentLocation
    }
}



GeneratePot $path