Ext.define('Scc.Portal.CustomizableForms.ToolboxItem', {
    extend: 'Ext.AbstractPlugin',
    alias: 'plugin.customizable.toolboxitem',
    requires: ['Scc.Portal.CustomizableForms.dd.ToolboxItemSource'],

    mixins: ['Ext.mixin.Observable'],

    config: {
        itemConfig: null
    },

    $configStrict: false,

    constructor: function(config) {
        var me = this;
        me.callParent(arguments);
        me.mixins.observable.constructor.call(me, config);
    },

    init: function(component) {
        var me = this;
        component.on('boxready', function () {
            new Scc.Portal.CustomizableForms.dd.ToolboxItemSource({
                sourceComponent: component,
                getSourceComponentConfig: function () {
                    return Ext.clone(me.getItemConfig());
                }
            });
        }, this, {single:true});
    }
});