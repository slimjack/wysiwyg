Ext.define('Scc.Portal.CustomizableForms.dd.ToolboxItemSource', {
    extend: 'Scc.Portal.CustomizableForms.dd.Source',
    requires: ['Scc.Portal.CustomizableForms.dd.InfoExtensions'],

    $configStrict: false,

    config: {
        sourceComponentConfig: null
    },

    describe: function(info) {
        this.callParent(arguments);
        Scc.Portal.CustomizableForms.dd.InfoExtensions.describeSourceConfig(info, this.getSourceComponentConfig());
    }
});