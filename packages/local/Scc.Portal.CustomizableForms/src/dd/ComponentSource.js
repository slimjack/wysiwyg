Ext.define('Scc.Portal.CustomizableForms.dd.ComponentSource', {
    extend: 'Scc.Portal.CustomizableForms.dd.Source',
    requires: [
        'Scc.Portal.CustomizableForms.dd.InfoExtensions',
        'Ext.Component'
    ],

    describe: function(info) {
        this.callParent(arguments);
        Scc.Portal.CustomizableForms.dd.InfoExtensions.describeSourceComponent(info, this.getSourceComponent());
    },

    beforeDragStart: function(info) {
        //This is rather a fix for cases when several sources are overlapped. The fix cancels drag for all sources except the most top
        var hoverEl = document.elementFromPoint(info.cursor.current.x, info.cursor.current.y);
        var hoverSourceComponent = Ext.Component.from(hoverEl, null, '[customizable]');
        return (this.getSourceComponent() === hoverSourceComponent) && this._isInContainer();
    },

    _isInContainer: function() {
        return !!this.getSourceComponent().up('[customizableCt]', 1);
    }
});