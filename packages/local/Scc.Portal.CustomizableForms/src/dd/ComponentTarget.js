Ext.define('Scc.Portal.CustomizableForms.dd.ComponentTarget', {
    extend: 'Ext.drag.Target',
    requires: [
        'Ext.Error'
    ],

    config: {
        targetComponent: null
    },

    constructor: function (cfg) {
        cfg.element = cfg.targetComponent.getEl();
        this.callParent([cfg]);
    },

    accepts: function (info) {
        var me = this;
        if (me.isDisabled()) {
            return false;
        }
        return info.isCustomizationDD
            && me.getTargetComponent().ownerCt.customizableCt
            && !info.sourceComponentContains(me.getTargetComponent())
            && me.getTargetComponent().ownerCt.acceptsComponent(info);
    },

    onDragMove: function (info) {
        var me = this;
        if (me.isDisabled()) {
            return;
        }
        if (me.accepts(info)) {
            info.showPlaceholder(me.$getPlaceholderCoordinates(info));
        } else {
            info.hidePlaceholder();
        }
    },

    onDrop: function (info) {
        var me = this;
        if (!me.isDisabled() && me.accepts(info)) {
            me.$acceptDroppedComponent(info);
        }
    },

    onDragLeave: function (info) {
        info.hidePlaceholder();
    },

    $acceptDroppedComponent: function (info) {
        var me = this;
        info.insertSourceNear(me.getTargetComponent());
    },

    $getPlaceholderCoordinates: function (info) {
        var me = this;
        var targetComponent = me.getTargetComponent();
        var relativeDropPosition = targetComponent.ownerCt.getRelativeDropPosition(
            info.cursor.current,
            targetComponent);
        switch (relativeDropPosition) {
            case 'left': 
                return {
                    x: targetComponent.getX(),
                    y: targetComponent.getY(),
                    height: targetComponent.getHeight()
                };
            case 'top': 
                return {
                    x: targetComponent.getX(),
                    y: targetComponent.getY(),
                    width: targetComponent.getWidth()
                };
            case 'right': 
                return {
                    x: targetComponent.getX() + targetComponent.getWidth(),
                    y: targetComponent.getY(),
                    height: targetComponent.getHeight()
                };
            case 'bottom': 
                return {
                    x: targetComponent.getX(),
                    y: targetComponent.getY() + targetComponent.getHeight(),
                    width: targetComponent.getWidth()
                };
            case 'center': 
                return {
                    x: targetComponent.getX(),
                    y: targetComponent.getY(),
                    width: targetComponent.getWidth(),
                    height: targetComponent.getHeight()
                };
            default:
                Ext.Error.raise('Unsupported relative drop position');
        }
    }
});