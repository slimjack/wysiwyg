Ext.define('Scc.Portal.CustomizableForms.dd.ContainerTarget', {
    extend: 'Scc.Portal.CustomizableForms.dd.ComponentTarget',

    accepts: function (info) {
        var me = this;
        if (me.isDisabled()) {
            return false;
        }
        return me.$canInsertAt(info) || me.callParent(arguments);
    },

    $acceptDroppedComponent: function (info) {
        var me = this;
        if (me.$canInsertAt(info)) {
            info.insertSourceInto(me.getTargetComponent());
        } else {
            me.callParent(arguments);
        }
    },

    $canInsertAt: function (info) {
        var me = this;
        return !info.sourceComponentEquals(me.getTargetComponent())
            && me.getTargetComponent().canInsertAt(info.cursor.current)
            && me.getTargetComponent().acceptsComponent(info);
    },

    $getPlaceholderCoordinates: function (info) {
        var me = this;
        if (me.$canInsertAt(info)) {
            return me.getTargetComponent().getInsertionPlaceholderCoordinates(info.cursor.current);
        }
        return me.callParent(arguments);
    }
});