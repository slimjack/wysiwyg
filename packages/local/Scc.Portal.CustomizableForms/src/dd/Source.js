Ext.define('Scc.Portal.CustomizableForms.dd.Source', {
    extend: 'Ext.drag.Source',
    requires: ['Scc.Portal.CustomizableForms.dd.InfoExtensions'],

    $configStrict: false,

    config: {
        sourceComponent: null
    },

    constructor: function (cfg) {
        cfg.element = (cfg.sourceComponent || cfg.getSourceComponent()).getEl();
        this.callParent(arguments);
        Ext.override(this, {   
            dragCleanup: function (info) {
                this.callParent(arguments);
                if (info) {
                    info.destroy();
                }
            }
        });
    },

    describe: function(info) {
        Scc.Portal.CustomizableForms.dd.InfoExtensions.applyPlaceholderApi(info);
    },

    $getDDProxyConfig: function () {
        return {
            width: this.getSourceComponent().getWidth(),
            height: this.getSourceComponent().getHeight()
        };
    },

    proxy: {
        type: 'placeholder',
        getElement: function(info) {
            var el = this.element;
            if (!el) {
                var proxyConfig = this.getSource().$getDDProxyConfig();
                this.element = el = Ext.getBody().createChild({
                    style: "width:" + proxyConfig.width + "px;height:" + proxyConfig.height + "px;border:1px dashed black;"
                });
            }
            el.show();
            return el;
        }
    },

    privates: {
        preventStart: function () {
            var me = this;
            var sourceComponent = me.getSourceComponent();
            if (sourceComponent.resizer && !sourceComponent.resizer.resizeTracker.mouseIsOut) {
                return true;
            }
            return me.callParent(arguments);
        }
    }
});
