Ext.define('Scc.Portal.CustomizableForms.dd.InfoExtensions', {
    statics: {
        describeSourceConfig: function (info, sourceComponentConfig) {
            this._applyBaseApi(info);
            Ext.override(info, {
                _sourceComponentConfig: sourceComponentConfig,

                sourceComponentContains: function (component) {
                    return false;
                },

                sourceComponentEquals: function (component) {
                    return false;
                },

                getSourceComponentDescriptor: function () {
                    return {
                        //TODO:modern/classic check for 'widget'
                        class: Ext.ClassManager.getByAlias('widget.' + this._sourceComponentConfig.xtype)
                    }
                },

                $pickSourceComponent: function () {
                    var me = this;
                    return Ext.widget(this._sourceComponentConfig);
                }
            });
        },

        describeSourceComponent: function (info, sourceComponent) {
            this._applyBaseApi(info);
            Ext.override(info, {
                _sourceComponent: sourceComponent,

                sourceComponentContains: function (component) {
                    return !!component.up(this._sourceComponent) || this.sourceComponentEquals(component);
                },

                sourceComponentEquals: function (component) {
                    return this._sourceComponent === component;
                },

                getSourceComponentDescriptor: function () {
                    return {
                        class: Ext.ClassManager.getClass(this._sourceComponent)
                    }
                },

                $pickSourceComponent: function () {
                    var me = this;
                    if (me._sourceComponent.ownerCt) {
                        me._sourceComponent.ownerCt.detachItem(me._sourceComponent);
                    }
                    return me._sourceComponent;
                }
            });
        },

        _applyBaseApi: function (info) {
            Ext.override(info, {
                isCustomizationDD: true,

                insertSourceInto: function (targetContainer) {
                    var me = this;
                    var insertionRegionName = targetContainer.getInsertionRegionName(me.cursor.current);
                    targetContainer.insertAt(insertionRegionName, me.$pickSourceComponent());
                },

                insertSourceNear: function (referenceComponent) {
                    var me = this;
                        //TODO:modern/classic check for 'ownerCt'
                        var referencePosition = referenceComponent.ownerCt.getRelativeDropPosition(me.cursor.current, referenceComponent);
                    referenceComponent.ownerCt.insertNear(referenceComponent, referencePosition, me.$pickSourceComponent());
                }
            });
        },

        applyPlaceholderApi: function (info) {
            Ext.override(info, {
                placeholderEl: Ext.getBody().createChild({
                    style: {
                        position: 'absolute',
                        backgroundColor: 'red',
                        zIndex: '10000'
                    } 
                }),

                destroy: function () {
                    this.placeholderEl.destroy();
                    this.callParent(arguments);
                },

                showPlaceholder: function (coordinates) {
                    var me = this;
                    var newPlaceholderCoordinates = Ext.apply({}, coordinates, { height: 3, width: 3 });
                    if (!coordinates.height) {//horizontal line
                        newPlaceholderCoordinates.y--;
                    } else if (!coordinates.width) {//vertical line
                        newPlaceholderCoordinates.x--;
                    } else {//rectangle
                        newPlaceholderCoordinates.x += 5;
                        newPlaceholderCoordinates.y += 5;
                        newPlaceholderCoordinates.width -= 10;
                        newPlaceholderCoordinates.height -= 10;
                    }
                    if (me.placeholderEl.getX() !== newPlaceholderCoordinates.x) {
                        me.placeholderEl.setX(newPlaceholderCoordinates.x)
                    }
                    if (me.placeholderEl.getY() !== newPlaceholderCoordinates.y) {
                        me.placeholderEl.setY(newPlaceholderCoordinates.y)
                    }
                    if (me.placeholderEl.getWidth() !== newPlaceholderCoordinates.width) {
                        me.placeholderEl.setWidth(newPlaceholderCoordinates.width)
                    }
                    if (me.placeholderEl.getHeight() !== newPlaceholderCoordinates.height) {
                        me.placeholderEl.setHeight(newPlaceholderCoordinates.height)
                    }
                    if (!me.placeholderEl.isVisible()) {
                        me.placeholderEl.show();
                    }
                    me._updatePlaceholderInsideTarget();
                },

                hidePlaceholder: function () {
                    var me = this;
                    me.placeholderEl.hide();
                },

                privates: {
                    _updatePlaceholderInsideTarget: function () {
                        var me = this;
                        var isPlaceholderInsideTarget = me.target
                            && me.placeholderEl.isVisible()
                            && me.target.getElement().getRegion().contains(me.placeholderEl.getRegion());
                        if (isPlaceholderInsideTarget) {
                            me.elementMap[me.placeholderEl.id] = me.target;
                        } else {
                            me.elementMap[me.placeholderEl.id] = undefined;
                        }
                    }
                }
            });
        }
    }
});