Ext.override(Ext.panel.Panel, {
    constructor: function () {
        var me = this;
        me.callParent(arguments);
        me.relayEvents(me, { dockedadd: 'itemsadd'});
    },

    $getItemsRegion: function () {
        var me = this;
        return me.body.getViewRegion();
    },

    canInsertAt: function (screenCoordinates) {
        var me = this;
        return me.callParent(arguments) || me._isInDockedInsertionRegion(Ext.util.Point.from(screenCoordinates));
    },

    acceptsComponent: function (info) {
        var me = this;
        if (me._isInDockedInsertionRegion(Ext.util.Point.from(info.cursor.current))) {
            return Ext.isClassOf(info.getSourceComponentDescriptor().class, Ext.Component);
        }
        return me.callParent(arguments);
    },

    eachItem: function (fn) {
        var me = this;
        me.callParent(arguments);
        me.dockedItems
            .filterBy(function (item) { return item !== me.header;})
            .each(fn);
    },

    detachItem: function (item) {
        var me = this;
        if (me.getDockedComponent(item)) {
            me.removeDocked(item, { destroy: false });
            delete item.dock;
        } else {
            me.callParent(arguments);
        }
    },

    insertNear: function (referenceComponent, referencePosition, componentToInsert) {
        var me = this;
        var dockedIndex = me.dockedItems.indexOf(referenceComponent);
        if (dockedIndex !== -1) {
            componentToInsert.dock = referenceComponent.dock;
            if (referencePosition === 'bottom' && (referenceComponent.dock === 'top' || referenceComponent.dock === 'bottom')
                || referencePosition === 'right' && (referenceComponent.dock === 'left' || referenceComponent.dock === 'right')) {
                dockedIndex++;
            }
            me.insertDocked(dockedIndex, componentToInsert);
        } else {
            me.callParent(arguments);
        }
    },

    insertAt: function (insertionRegionName, componentToInsert) {
        var me = this;
        switch (insertionRegionName) {
            case 'topDocked': componentToInsert.dock = 'top'; me.addDocked(componentToInsert); break;
            case 'leftDocked': componentToInsert.dock = 'left'; me.addDocked(componentToInsert); break;
            case 'bottomDocked': componentToInsert.dock = 'bottom'; me.addDocked(componentToInsert, 0); break;
            case 'rightDocked': componentToInsert.dock = 'right'; me.addDocked(componentToInsert, 0); break;
            default: me.callParent(arguments);
        }
    },

    getInsertionRegionName: function (screenCoordinates) {
        var me = this;
        var screenPoint = Ext.util.Point.from(screenCoordinates);
        if (me._isInDockedInsertionRegion(screenPoint)) {
            return me.body.getRegion().getSubregionName(screenPoint) + 'Docked';
        }
        return me.callParent(arguments);
    },

    getInsertionPlaceholderCoordinates: function (screenCoordinates) {
        var me = this;
        var screenPoint = Ext.util.Point.from(screenCoordinates);
        if (me._isInDockedInsertionRegion(screenPoint)) {
            var dockedInsertionRegion = me.body.getRegion();
            var hoverSubregion = dockedInsertionRegion.getSubregionName(screenPoint);
            switch (hoverSubregion) {
                case 'top':
                    return {
                        x: dockedInsertionRegion.left,
                        y: dockedInsertionRegion.top + 3,
                        width: dockedInsertionRegion.width
                    };
                case 'left':
                    return {
                        x: dockedInsertionRegion.left + 3,
                        y: dockedInsertionRegion.top,
                        height: dockedInsertionRegion.height
                    };
                case 'bottom':
                    return {
                        x: dockedInsertionRegion.left,
                        y: dockedInsertionRegion.top + dockedInsertionRegion.height - 3,
                        width: dockedInsertionRegion.width
                    };
                case 'right':
                    return {
                        x: dockedInsertionRegion.left + dockedInsertionRegion.width - 3,
                        y: dockedInsertionRegion.top,
                        height: dockedInsertionRegion.height
                    };
                default:
                    Ext.Error.raise('Unsupported region: ' + hoverSubregion);
            }
        }
        return me.callParent(arguments);
    },

    getRelativeDropPosition: function (cursor, referenceComponent) {
        var me = this;
        if (me.getDockedComponent(referenceComponent)) {
            var referenceRegion = referenceComponent.getRegion();
            var cursorPoint = Ext.util.Point.from(cursor);
            var isHorizontalOrientation = referenceComponent.dock === 'left' || referenceComponent.dock === 'right';
            return referenceRegion.getHalfregionName(cursorPoint, isHorizontalOrientation);
        }
        return me.callParent(arguments);
    },

    $onDesignerModeEnter: function () {
        var me = this;
        if (me._customizableDDTarget) {
            me._customizableDDTarget.enable();
            me._originalStylesBeforeDesignerMode = {
                padding: me.body.getStyle('padding'),
                outline: me.body.getStyle('outline'),
                outlineOffset: me.body.getStyle('outlineOffset')
            };
            me.setBodyStyle({
                padding: '5px',
                outline: '1px dashed black',
                outlineOffset: '-3px'
            });
            me.$setMinSize(100, 50);
        }
        me.updateLayout();
    },

    $onDesignerModeLeave: function () {
        var me = this;
        if (me._customizableDDTarget) {
            me._customizableDDTarget.disable();
            me.setBodyStyle(me._originalStylesBeforeDesignerMode);
            delete me._originalStylesBeforeDesignerMode;
            me.$resetMinSize();
        }
        me.updateLayout();
    },

    privates: {
        _isInDockedInsertionRegion: function(screenPoint) {
            var me = this;
            return me.body.getRegion().contains(screenPoint)
                && !me.body.getViewRegion().contains(screenPoint);
        }
    }
});