Ext.override(Ext.layout.Layout, {
    onOwnerDesignerModeEntered: Ext.emptyFn,
    onOwnerDesignerModeLeaved: Ext.emptyFn,
    $onOwnerReleased: Ext.emptyFn,
    $onOwnerAssigned: Ext.emptyFn,

    setOwner: function (owner) {
        var me = this;
        if (me.owner !== owner) {
            if (me.owner) {
                me.$onOwnerReleased(me.owner);
            }
            if (owner) {
                me.$onOwnerAssigned(me.owner);
            }
        }
        me.callParent(arguments);
    }
});