Ext.override(Ext.layout.container.Border, {
    getRelativeDropPosition: function (cursor, referenceComponent) {
        var me = this;
        var referenceRegion = referenceComponent.getRegion();
        var cursorPoint = Ext.util.Point.from(cursor);
        switch (referenceComponent.region) {
            case 'north':
            case 'south': 
                return referenceRegion.isInTopHalfregion(cursorPoint) ? 'top' : 'bottom';
            case 'west':
            case 'east': 
                return referenceRegion.isInLeftHalfregion(cursorPoint) ? 'left' : 'right';
            case 'center':
                return referenceComponent.itemId === 'fakeCenter'
                    ? 'center'
                    : referenceRegion.getSubregionName(cursorPoint);
            default:
                return me.callParent(arguments);
        }
    },

    afterRemove: function (item) {
        var me = this;
        if (item.region === 'center' && item.itemId !== 'fakeCenter' && me.owner.isDesignerMode()) {
            me._addFakeCenter(me.owner);
        }
        me.callParent(arguments);
    },

    add: function (componentToInsert) {
        var me = this;
        if (!componentToInsert.region && !me._hasCenter(me.owner)) {
            componentToInsert.region = 'center';
        }
        if (componentToInsert.region === 'center' && me._hasFakeCenter(me.owner)) {
            me._removeFakeCenter(me.owner);
        }
        me.owner.add(componentToInsert);
    },

    insertNear: function (referenceComponent, referencePosition, componentToInsert) {
        var me = this;
        if (referenceComponent.itemId === 'fakeCenter') {
            componentToInsert.region = 'center';
            var insertionIndex = me.owner.items.indexOf(referenceComponent);
            me._removeFakeCenter(me.owner);
            me.owner.insert(insertionIndex, componentToInsert);
        } else {
            componentToInsert.region = referenceComponent.region === 'center'
                ? me._referencePositionToBorderRegion(referencePosition)
                : referenceComponent.region;
            me.callParent(arguments);
        }
    },

    onOwnerDesignerModeEntered: function () {
        var me = this;
        if (!me._hasFakeCenter(me.owner) && !me._hasCenter(me.owner)) {
            me._addFakeCenter(me.owner);
        }
        me.callParent(arguments);
    },

    onOwnerDesignerModeLeaved: function () {
        var me = this;
        if (me._hasFakeCenter(me.owner)) {
            me._removeFakeCenter(me.owner);
        }
        me.callParent(arguments);
    },

    $onOwnerReleased: function (owner) {
        var me = this;
        if (me._hasFakeCenter(owner)) {
            me._removeFakeCenter(owner);
        }
        me.callParent(arguments);
    },

    $onOwnerAssigned: function (owner) {
        var me = this;
        if (!me._hasFakeCenter(owner) && !me._hasCenter(owner) && owner.isDesignerMode()) {
            me._addFakeCenter(me.owner);
        }
        me.callParent(arguments);
    },

    $restoreItemState: function (item, originalState) {
        var me = this;
        delete item.region;
        me.callParent(arguments);
    },

    privates: {
        _referencePositionToBorderRegion: function (referencePosition) {
            switch (referencePosition) {
                case 'left': return 'west';
                case 'top': return 'north';
                case 'right': return 'east';
                case 'bottom':  return 'south';
                case 'center':  return 'center';
                default:
                    Ext.Error.raise('Unsupported reference position: ' + referencePosition);
            }
        },

        _addFakeCenter: function (container) {
            container.add({
                xtype: 'component',
                region: 'center',
                style: {
                    margin: '3px',
                    border: '1px dashed black'
                },
                itemId: 'fakeCenter',
                minWidth: 20,
                minHeight: 20
            });
        },
    
        _hasCenter: function (container) {
            return !!container.items.findBy(function (item) { return item.region === 'center' && item.itemId !== 'fakeCenter'; });
        },
    
        _hasFakeCenter: function (container) {
            return !!container.items.findBy(function (item) { return item.region === 'center' && item.itemId === 'fakeCenter'; });
        },
    
        _removeFakeCenter: function (container) {
            var fakeCenter = container.getComponent('fakeCenter');
            if (fakeCenter) {
                container.remove(fakeCenter);
            }
        }
    }
});