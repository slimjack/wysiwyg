Ext.override(Ext.layout.container.Container, {
    isHorizontalArrangement: function () {
        return false;//by default according to css spec block elements are arranged vertically
    },

    acceptsComponent: function (componentClass) {
        return Ext.isClassOf(componentClass, Ext.Component);
    },

    insertNear: function (referenceComponent, referencePosition, componentToInsert) {
        var me = this;
        var insertionIndex = me.owner.items.indexOf(referenceComponent);
        if (referencePosition === 'right' || referencePosition === 'bottom') {
            insertionIndex++;
        }
        me.owner.insert(insertionIndex, componentToInsert);
    },

    add: function (componentToInsert) {
        var me = this;
        me.owner.add(componentToInsert);
    },

    getRelativeDropPosition: function (cursor, referenceComponent) {
        var me = this;
        var referenceRegion = referenceComponent.getRegion();
        var cursorPoint = Ext.util.Point.from(cursor);
        return referenceRegion.getHalfregionName(cursorPoint, me.isHorizontalArrangement());
    },

    onAdd: function (item) {
        var me = this;
        me._captureItemState(item);
        me.callParent(arguments);
    },

    afterRemove: function (item) {
        var me = this;
        me.callParent(arguments);
        me._restoreItemState(item);
    },

    $onOwnerReleased: function (owner) {
        var me = this;
        owner.items.each(me._restoreItemState, me);
        me.callParent(arguments);
    },

    $onOwnerAssigned: function (owner) {
        var me = this;
        owner.items.each(me._captureItemState, me);
        me.callParent(arguments);
    },

    $captureItemState: function () {
        return {};
    },

    $restoreItemState: function (item, originalState) {
        Ext.apply(item, originalState);
    },

    privates: {
        _captureItemState: function (item) {
            var me = this;
            //if item is instantiated and has not been captured yet
            if (item.isComponent && !item._itemStateBeforeLayout) {
                item._itemStateBeforeLayout = me.$captureItemState(item)
            }
        },

        _restoreItemState: function (item) {
            var me = this;
            if (item._itemStateBeforeLayout) {
                me.$restoreItemState(item, item._itemStateBeforeLayout);
                delete item._itemStateBeforeLayout;
            }
        }
    }
});