Ext.override(Ext.Component, {
    requires: [
        'Scc.Portal.CustomizableForms.dd.ComponentTarget',
        'Scc.Portal.CustomizableForms.dd.ComponentSource'
    ],

    config: {
        designerMode: false
    },

    initComponent: function () {
        var me = this;
        me.on('boxready', function () {
            me._isBoxReady = true;
        }, me, { single:true });
        me.callParent(arguments);
        if (me.customizable) {
            me.onceReady(me._initDD);
        }
    },

    _initDD: function () {
        var me = this;
        me._customizableDDSource = me.$createDDSource();
        me._customizableDDTarget = me.$createDDTarget();
        if (!me.isDesignerMode()) {
            me._customizableDDSource.disable();
            me._customizableDDSource.disable();
        }
    },

    $createDDSource: function () {
        var me = this;
        return new Scc.Portal.CustomizableForms.dd.ComponentSource({ sourceComponent: me });
    },

    $createDDTarget: function () {
        var me = this;
        return new Scc.Portal.CustomizableForms.dd.ComponentTarget({ targetComponent: me });
    },

    onceReady: function (fn, scope) {
        var me = this;
        if (me._isBoxReady) {
            fn.call(scope || me);
        } else {
            me.on('boxready', fn, scope || me, { single:true });
        }
    },

    updateDesignerMode: function (designerMode, oldDesignerMode) {
        var me = this;
        if (designerMode) {
            me.$onDesignerModeEnter();
            //me.$setResizable();
        } else if (oldDesignerMode) {
            me.$onDesignerModeLeave();
            //me.$resetResizable();
        }
        me.updateLayout();
    },

    isDesignerMode: function () {
        var me = this;
        return me.getDesignerMode();
    },

    $setResizable: function () {
        var me = this;
        if (!me._originallyResizable) {
            me.initResizable(true);
        }
    },

    $resetResizable: function () {
        var me = this;
        if (!me._originallyResizable) {
            me.resizer.destroy();
            delete me.resizer;
        }
    },

    $onDesignerModeEnter: function () {
        var me = this;
        if (me._customizableDDSource) {
            me._customizableDDSource.enable();
        }
        if (me._customizableDDTarget) {
            me._customizableDDTarget.enable();
        }
    },

    $onDesignerModeLeave: function () {
        var me = this;
        if (me._customizableDDSource) {
            me._customizableDDSource.disable();
        }
        if (me._customizableDDTarget) {
            me._customizableDDTarget.disable();
        }
    }
});
