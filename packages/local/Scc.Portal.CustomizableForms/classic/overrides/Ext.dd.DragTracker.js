Ext.override(Ext.dd.DragTracker, {
    onMouseOver: function () {
        var me = this;
        if (!Ext.drag.Manager.isDragging) {
            me.callParent(arguments);
        }
    }   
});
