Ext.override(Ext.layout.container.Accordion, {
    statics: {
        affectedFields: [
            'ignoreHeaderBorderManagement',
            'animCollapse',
            'bodyAriaRole',
            'collapsible',
            'collapseFirst',
            'hideCollapseTool',
            'titleCollapse',
            'headerOverCls',
            'hideHeader',
            'title'
        ]
    },

    acceptsComponent: function (componentClass) {
        return Ext.isClassOf(componentClass, Ext.panel.Panel);
    },
 
    beforeRenderItems: function(items) {
        var me = this;
        for (var i = 0; i < items.length; i++) {
            item = items[i];
            
            if (item.rendered && !item.isAccordionPanel) {
                // Set up initial properties for Panels in an accordion.
                item.isAccordionPanel = true;
                item.bodyAriaRole = 'tabpanel';
                item.accordionWrapOver = me.wrapOver;
                
                if (!me.multi || item.collapsible !== false) {
                    item.setCollapsible(false);
                    if (Ext.isDefined(me.collapseFirst)) {
                        item.collapseFirst = me.collapseFirst;
                    }
 
                    if (me.hideCollapseTool) {
                        item.hideCollapseTool = me.hideCollapseTool;
                        item.titleCollapse = true;
                    } else if (me.titleCollapse && item.titleCollapse === undefined) {
                        item.titleCollapse = me.titleCollapse;
                    }
                }
 
                item.hideHeader = item.width = null;
                item.title = item.title || '&#160;';
                item.addBodyCls(Ext.baseCSSPrefix + 'accordion-body');
 
                // If only one child Panel is allowed to be expanded
                // then collapse all except the first one found with collapsed:false
                // If we have hasExpanded set, we've already done this
                if (!me.multi) {
                    item.collapsed = true;
                    // If only one child Panel may be expanded, then intercept expand/show requests.
                    me.owner.mon(item, 'show', me.onComponentShow, me);
                }
 
                // Need to still check this outside multi because we don't want
                // a single item to be able to collapse
                item.headerOverCls = Ext.baseCSSPrefix + 'accordion-hd-over';
                debugger;
                item.setCollapsible(true);
            }
        }
        me.callParent(arguments);
    },

    $captureItemState: function (item) {
        var me = this;
        return  Ext.copy(
            me.callParent(arguments),
            item,
            Ext.layout.container.Accordion.affectedFields);
    },

    $restoreItemState: function (panel, originalState) {
        var me = this;
        panel.setCollapsible(originalState.collapsible);
        delete originalState.collapsible;
        panel.header.removeCls(Ext.baseCSSPrefix + 'accordion-hd');
        panel.header.removeCls(Ext.baseCSSPrefix + 'accordion-hd-over');
        panel.header.removeCls(Ext.baseCSSPrefix + 'accordion-hd-sibling-expanded');
        panel.header.removeCls(Ext.baseCSSPrefix + 'accordion-hd-last-collapsed');
        panel.removeBodyCls(Ext.baseCSSPrefix + 'accordion-body');
        delete panel.isAccordionPanel;
        delete panel.accordionWrapOver;
        me.owner.mun(panel, 'show', me.onComponentShow, me);
        me.callParent(arguments);
    }
});