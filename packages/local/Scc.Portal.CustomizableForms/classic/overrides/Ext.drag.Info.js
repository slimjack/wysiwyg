Ext.override(Ext.drag.Info, {
    constructor: function () {
        var me = this;
        me.callParent(arguments);
        Ext.drag.Manager.isDragging = true;
        if (!Ext.drag.Manager.isDragging) {
        }
    },
    
    destroy: function () {
        var me = this;
        Ext.drag.Manager.isDragging = false;
        me.callParent(arguments);
    }
});
