Ext.override(Ext.container.Container, {
    requires: [
        'Scc.Portal.CustomizableForms.dd.ContainerTarget'
    ],

    initComponent: function () {
        var me = this;
        if (me.customizableCt) {
            me.customizable = true;
        }
        me.callParent(arguments);
        if (me.resizable) {
            me._originallyResizable = true;
        }
        me._itemsAvailable = true;
        me._setDesignerModeForItems(me.getDesignerMode());
        me.on('add', function(_, item) { me.$onItemAdded(item); } , me);
    },

    $createDDTarget: function () {
        var me = this;
        if (me.customizableCt) {
            return new Scc.Portal.CustomizableForms.dd.ContainerTarget({ targetComponent: me });
        } else {
            return me.callParent();
        }
    },

    $onItemAdded: function (item) {
        var me = this;
        item.setDesignerMode(me.getDesignerMode());
    },

    updateDesignerMode: function (designerMode, oldDesignerMode) {
        var me = this;
        if (designerMode) {
            me.getLayout().onOwnerDesignerModeEntered();
        } else if (oldDesignerMode) {
            me.getLayout().onOwnerDesignerModeLeaved();
        }
        me.callParent(arguments);
        me._setDesignerModeForItems(designerMode);
    },

    $getItemsRegion: function () {
        var me = this;
        return me.getEl().getViewRegion();
    },

    canInsertAt: function (screenCoordinates) {
        var me = this;
        return !me.items.getCount() && me.$getItemsRegion().contains(Ext.util.Point.from(screenCoordinates));
    },

    insertNear: function (referenceComponent, referencePosition, componentToInsert) {
        var me = this;
        if (me.items.indexOf(referenceComponent) !== -1) {
            me.getLayout().insertNear(referenceComponent, referencePosition, componentToInsert);
        } else {
            Ext.Error.raise('Unable to insert near the component which is not in the container\'s "items"');
        }
    },

    insertAt: function (insertionRegionName, componentToInsert) {
        var me = this;
        if (insertionRegionName === 'items') {
            me.getLayout().add(componentToInsert);
        } else {
            Ext.Error.raise('Unsupported insertion region name: "' + insertionRegionName + '".');
        }
    },

    eachItem: function (fn) {
        var me = this;
        me.items.each(fn);
    },

    detachItem: function (item) {
        var me = this;
        if (me.items.indexOf(item) !== -1) {
            me.remove(item, { destroy: false });
        } else {
            Ext.Error.raise('Unable to detach not owned item');
        }
    },

    getInsertionRegionName: function (cursor) {
        return 'items';
    },

    getInsertionPlaceholderCoordinates: function (screenCoordinates) {
        var me = this;
        var itemsRegion = me.$getItemsRegion();
        if (me.getLayout().isHorizontalArrangement()) {
            return {
                x: (itemsRegion.left + itemsRegion.right)/2,
                y: itemsRegion.top,
                height: itemsRegion.height
            };
        } else {
            return {
                x: itemsRegion.left,
                y: (itemsRegion.top + itemsRegion.bottom)/2,
                width: itemsRegion.width
            };
        }
    },

    getRelativeDropPosition: function (cursor, referenceComponent) {
        var me = this;
        return me.getLayout().getRelativeDropPosition(cursor, referenceComponent);
    },

    acceptsComponent: function (info) {
        var me = this;
        var componentClass = info.getSourceComponentDescriptor().class
        return me.getLayout().acceptsComponent(componentClass);
    },

    isHorizontalItemsArrangement: function () {
        var me = this;
        return me.getLayout().isHorizontalArrangement();
    },

    $onDesignerModeEnter: function () {
        var me = this;
        if (me.customizable) {
            me._originalStylesBeforeDesignerMode = {
                padding: me.getEl().getStyle('padding'),
                outline: me.getEl().getStyle('outline'),
                outlineOffset: me.getEl().getStyle('outlineOffset')
            };
            me.setStyle({
                padding: '5px',
                outline: '1px dashed black',
                outlineOffset: '-3px'
            });
            me.$setMinSize(30, 30);
        }
        me.callParent(arguments);
    },

    $onDesignerModeLeave: function () {
        var me = this;
        if (me.customizable) {
            me.setStyle(me._originalStylesBeforeDesignerMode);
            delete me._originalStylesBeforeDesignerMode;
            me.$resetMinSize();
        }
        me.callParent(arguments);
    },

    $setMinSize: function (minWidth, minHeight) {
        var me = this;
        me._originalMinHeight = me.getMinHeight();
        me._originalMinWidth = me.getMinWidth();
        me.setMinHeight(Math.max(minHeight, me._originalMinHeight));
        me.setMinWidth(Math.max(minWidth, me._originalMinWidth));
    },

    $resetMinSize: function () {
        var me = this;
        me.setMinHeight(me._originalMinHeight);
        me.setMinWidth(me._originalMinWidth);
        delete me._originalMinHeight;
        delete me._originalMinWidth;
    },

    privates: {
        _setDesignerModeForItems: function (designerMode) {
            var me = this;
            if (me._itemsAvailable) {
                me.eachItem(function (item) { item.setDesignerMode(designerMode); })
            }
        }
    }
});