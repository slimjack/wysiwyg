Ext.override(Ext.util.Region, {
    getHalfregionName: function (point, isHorizontalOrientation) {
        var me = this;
        if (isHorizontalOrientation) {
            return me.isInLeftHalfregion(point) ? 'left' : 'right';
        } else {
            return me.isInTopHalfregion(point) ? 'top' : 'bottom';
        }    
    },

    isInTopHalfregion: function (point) {
        var me = this;
        return me.contains(point) && point.y < (me.top + me.height/2);
    },

    isInLeftHalfregion: function (point) {
        var me = this;
        return me.contains(point) && point.x < (me.left + me.width/2);
    },

    isInTopLeftHalfregion: function (point) {
        var me = this;
        return me.contains(point) && point.y < (me.top + me.height - (point.x - me.left) * me.height / me.width);
    },

    isInBottomLeftHalfregion: function (point) {
        var me = this;
        return me.contains(point) && point.y > (me.top + (point.x - me.left) * me.height / me.width);
    },

    getSubregionName: function (point) {
        var me = this;
        var isInTopLeftHalfregion = me.isInTopLeftHalfregion(point);
        var isInBottomLeftHalfregion = me.isInBottomLeftHalfregion(point);
        if (isInTopLeftHalfregion) {
            return isInBottomLeftHalfregion
                ? 'left'
                : 'top';
        } else {
            return isInBottomLeftHalfregion
                ? 'bottom'
                : 'right';
        } 
    }
});