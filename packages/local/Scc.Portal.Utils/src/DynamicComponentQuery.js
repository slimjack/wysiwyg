﻿Ext.define('Scc.Portal.Utils.DynamicComponentQuery', {
    alternateClassName: 'DynamicComponentQuery',
    requires: [
        'Ext.util.Observable',
        'Ext.Error',
        'Ext.Array',
        'Ext.Object'
    ],
    _defaultMethods: ['disable', 'enable', 'setReadOnly', 'setDisabled'],
    _defaultEvents: [],
    mixins: ['Ext.util.Observable'],
    constructor: function (config, query, excludeQuery) {
        var me = this;
        if (config instanceof Ext.AbstractComponent) {
            config = {
                view: config,
                query: query,
                excludeQuery: excludeQuery
            };
        }
        Ext.apply(me, config);
        me.mixins.observable.constructor.call(me, config);
        if (!me.query) {
            Ext.Error.raise('"query" is not specified');
        }
        if (!me.view) {
            Ext.Error.raise('"view" is not specified');
        }
        if (!(me.view instanceof Ext.AbstractComponent)) {
            Ext.Error.raise('"view" is not an instance of Ext.AbstractComponent');
        }

        me._everyDelegates = [];
        me._everyRemovedDelegates = [];
        me._eventRelayers = {};
        me._events = Ext.Array.union(me._defaultEvents, Ext.Array.from(me.events));
        delete me.events;
        me._relayComponentsEvents();

        var methods = Ext.Array.union(me._defaultMethods, Ext.Array.from(me.methods));
        delete me.methods;
        me._createProxyMethods(methods);

        me.view.on('destroy', me._onViewDestroyed, me);
        if (me.view.isContainer) {
            me._subscribeOnLayoutChange();
        }
    },

    //region Public methods
    destroy: function () {
        var me = this;
        me._isDestroyed = true;
        me._everyDelegates = [];
        me._everyRemovedDelegates = [];
        Ext.Object.eachValue(me._eventRelayers, function (relayer) {
            Ext.destroy(relayer);
        });
        me._eventRelayers = {};
        me.clearListeners();
        me.callParent(arguments);
    },

    each: function (fn) {
        var me = this;
        Ext.Array.each(me._select(), fn);
    },

    contains: function (item) {
        var me = this;
        return Ext.Array.contains(me._select(), item);
    },

    isEmpty: function () {
        var me = this;
        return !(me._select().length);
    },

    invoke: function (method) {
        var me = this;
        var args = Array.prototype.slice.call(arguments, 1);
        var result = null;
        Ext.Array.forEach(me._select(), function (component) {
            if (Ext.isFunction(component[method])) {
                result = component[method].apply(component, args);
            }
        });
        return result;
    },

    every: function (delegate) {
        var me = this;
        Ext.Array.include(me._everyDelegates, delegate);
        me.each(delegate);
    },

    everyRemoved: function (delegate) {
        var me = this;
        Ext.Array.include(me._everyRemovedDelegates, delegate);
    },
    //endregion

    //region Private methods
    privates: {
        _onViewDestroyed: function () {
            var me = this;
            me.destroy();
        },

        _relayComponentsEvents: function () {
            var me = this;
            me.every(function (component) {
                me._eventRelayers[component.id] = me.relayEvents(component, me._events);
            });
            me.everyRemoved(function (component) {
                Ext.destroy(me._eventRelayers[component.id]);
                delete me._eventRelayers[component.id];
            });
        },

        _createProxyMethods: function (methods) {
            var me = this;
            Ext.Array.forEach(methods, function (method) {
                if (!me[method]) {
                    me[method] = Ext.bind(me.invoke, me, [method], 0);
                }
            });
        },

        _select: function () {
            var me = this;
            if (!me._selectedComponents) {
                me._selectedComponents = me.view.query(me.query);
                if (me.excludeQuery) {
                    me._selectedComponents = Ext.Array.difference(me._selectedComponents, me.view.query(me.excludeQuery));
                }
            }
            return me._selectedComponents;
        },

        _subscribeOnLayoutChange: function () {
            var me = this;
            if (!me._onComponentAddedIdleThrottled) {
                me._onComponentAddedIdleThrottled = Ext.createIdleThrottled(me._onComponentAdded);
            }
            if (!me._onComponentRemovedIdleThrottled) {
                me._onComponentRemovedIdleThrottled = Ext.createIdleThrottled(me._onComponentRemoved);
            }
            var containers = me.view.query('[isContainer]');
            containers.push(me.view);
            Ext.Array.forEach(containers, function (container) {
                container.on('add', me._onComponentAddedIdleThrottled, me);
                container.on('remove', me._onComponentRemovedIdleThrottled, me);
            });
        },

        _onComponentAdded: function () {
            var me = this;
            if (me.view.isContainer) {
                me._subscribeOnLayoutChange();
            }
            var oldComponents = me._selectedComponents;
            me._selectedComponents = null;
            var newComponents = me._select();
            var addedComponents = Ext.Array.difference(newComponents, oldComponents);
            if (addedComponents.length) {
                me._applyEvery(addedComponents);
                me.fireEvent('componentsadded', addedComponents);
            }
        },

        _onComponentRemoved: function () {
            var me = this;
            var oldComponents = me._selectedComponents;
            me._selectedComponents = null;
            var newComponents = me._select();
            var removedComponents = Ext.Array.difference(oldComponents, newComponents);
            if (removedComponents.length) {
                me._applyEveryRemoved(removedComponents);
                me.fireEvent('componentsremoved', removedComponents);
            }
        },

        _applyEvery: function (components) {
            var me = this;
            Ext.Array.forEach(me._everyDelegates, function (delegate) {
                Ext.Array.forEach(components, delegate);
            });
        },

        _applyEveryRemoved: function (components) {
            var me = this;
            Ext.Array.forEach(me._everyRemovedDelegates, function (delegate) {
                Ext.Array.forEach(components, delegate);
            });
        }
    }
    //endregion
});
