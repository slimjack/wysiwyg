﻿Ext.define('Scc.Portal.Globalization.DateTimeFormatField', {
    extend: 'Ext.form.field.Picker',
    alias: 'widget.dateTimeFormatField',
    nonConfigItemField: true,
    gridConfig: {
        minHeight: 100,
        maxHeight: 300,
        height: 200
    },
    columns: [{
        text: ExtJSTexts.t('formatColumn'),
        dataIndex: 'Format'
    }, {
        text: ExtJSTexts.t('exampleColumn'),
        dataIndex: 'Example',
        flex: 1
    }],
    displayField: 'Format',
    valueField: 'Format',
    maxLength: 128,

    initComponent: function () {
        var me = this;
        var data = [];
        var predefinedFormats = me._getPredefinedFormats();

        if (me.formatType) {
            predefinedFormats.each(function (key, value) {
                if (value.type === me.formatType) {
                    data.push({
                        Format: key,
                        Example: value.example,
                        FormatType: value.type
                    });
                }
            });
        } else {
            predefinedFormats.each(function (key, value) {
                data.push({
                    Format: key,
                    Example: value.example,
                    FormatType: value.type
                });
            });
        }

        this.store = Ext.create('Ext.data.Store', {
            fields: ['Format', 'Example', 'FormatType'],
            data: data
        });

        this.callParent(arguments);
    },

    createPicker: function () {
        var config = Ext.applyIf({
            scrollable: true,
            floating: true,
            store: this.store,
            columns: this.columns,
            listeners: {
                selectionchange: {
                    fn: function (grid, selectedRecord) {
                        this.setRecord(selectedRecord);
                    },
                    scope: this
                }
            }
        }, this.gridConfig);
        var grid = Ext.create('Ext.grid.Panel', config);
        return grid;
    },

    setRecord: function (record) {
        var format = record[0].get(this.displayField);
        this.setValue(format);
        this.collapse();
    },

    formatIsValid: function (value) {
        if (!Scc.Portal.Globalization.SupportedDateTimeFormat.isValid(value)) {
            return false;
        }

        var result = true;
        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        if (globalize.isUnicodeFormat(value)) {
            switch (this.formatType) {
                case 'date':
                    result = globalize.formatContainsDateInfo(value) && !globalize.formatContainsTimeInfo(value);
                    break;
                case 'datetime':
                    result = globalize.formatContainsDateInfo(value);
                    break;
                case 'time':
                    result = !globalize.formatContainsDateInfo(value) && globalize.formatContainsTimeInfo(value);
                    break;
            }
        } else {
            switch (this.formatType) {
                case 'date':
                    result = Ext.Date.formatContainsDateInfo(value) && !Ext.Date.formatContainsHourInfo(value);
                    break;
                case 'datetime':
                    result = Ext.Date.formatContainsDateInfo(value);
                    break;
                case 'time':
                    result = !Ext.Date.formatContainsDateInfo(value) && Ext.Date.formatContainsHourInfo(value);
                    break;
            }
        }

        return result;
    },

    validator: function (value) {
        return !value || this.formatIsValid(value) ?  true : ExtJSTexts.t('formatWrongMessage');
    },

    privates: {
        _getPredefinedFormats: function () {
            var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
            var map = new Ext.util.HashMap();

            map.add('t-full', { type: 'time', example: globalize.dateFormatter({ time: "full" })(new Date()) });
            map.add('t-long', { type: 'time', example: globalize.dateFormatter({ time: "long" })(new Date()) });
            map.add('t-medium', { type: 'time', example: globalize.dateFormatter({ time: "medium" })(new Date()) });
            map.add('t-short', { type: 'time', example: globalize.dateFormatter({ time: "short" })(new Date()) });

            map.add('d-full', { type: 'date', example: globalize.dateFormatter({ date: "full" })(new Date()) });
            map.add('d-long', { type: 'date', example: globalize.dateFormatter({ date: "long" })(new Date()) });
            map.add('d-medium', { type: 'date', example: globalize.dateFormatter({ date: "medium" })(new Date()) });
            map.add('d-short', { type: 'date', example: globalize.dateFormatter({ date: "short" })(new Date()) });

            map.add('dt-full', { type: 'datetime', example: globalize.dateFormatter({ datetime: "full" })(new Date()) });
            map.add('dt-long', { type: 'datetime', example: globalize.dateFormatter({ datetime: "long" })(new Date()) });
            map.add('dt-medium', { type: 'datetime', example: globalize.dateFormatter({ datetime: "medium" })(new Date()) });
            map.add('dt-short', { type: 'datetime', example: globalize.dateFormatter({ datetime: "short" })(new Date()) });

            map.add('sk-h', { type: 'time', example: globalize.dateFormatter({ skeleton: "h" })(new Date()) });
            map.add('sk-H', { type: 'time', example: globalize.dateFormatter({ skeleton: "H" })(new Date()) });
            map.add('sk-hm', { type: 'time', example: globalize.dateFormatter({ skeleton: "hm" })(new Date()) });
            map.add('sk-Hm', { type: 'time', example: globalize.dateFormatter({ skeleton: "Hm" })(new Date()) });
            map.add('sk-hms', { type: 'time', example: globalize.dateFormatter({ skeleton: "hms" })(new Date()) });
            map.add('sk-Hms', { type: 'time', example: globalize.dateFormatter({ skeleton: "Hms" })(new Date()) });
            map.add('sk-ms', { type: 'time', example: globalize.dateFormatter({ skeleton: "ms" })(new Date()) });

            map.add('sk-M', { type: 'date', example: globalize.dateFormatter({ skeleton: "M" })(new Date()) });
            map.add('sk-Md', { type: 'date', example: globalize.dateFormatter({ skeleton: "Md" })(new Date()) });
            map.add('sk-MEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "MEd" })(new Date()) });
            map.add('sk-MMM', { type: 'date', example: globalize.dateFormatter({ skeleton: "MMM" })(new Date()) });
            map.add('sk-MMMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "MMMd" })(new Date()) });
            map.add('sk-MMMEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "MMMEd" })(new Date()) });
            map.add('sk-Gy', { type: 'date', example: globalize.dateFormatter({ skeleton: "Gy" })(new Date()) });
            map.add('sk-GyMMM', { type: 'date', example: globalize.dateFormatter({ skeleton: "GyMMM" })(new Date()) });
            map.add('sk-GyMMMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "GyMMMd" })(new Date()) });
            map.add('sk-GyMMMEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "GyMMMEd" })(new Date()) });
            map.add('sk-d', { type: 'date', example: globalize.dateFormatter({ skeleton: "d" })(new Date()) });
            map.add('sk-E', { type: 'date', example: globalize.dateFormatter({ skeleton: "E" })(new Date()) });
            map.add('sk-Ed', { type: 'date', example: globalize.dateFormatter({ skeleton: "Ed" })(new Date()) });
            map.add('sk-MMMMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "MMMMd" })(new Date()) });
            map.add('sk-y', { type: 'date', example: globalize.dateFormatter({ skeleton: "y" })(new Date()) });
            map.add('sk-yM', { type: 'date', example: globalize.dateFormatter({ skeleton: "yM" })(new Date()) });
            map.add('sk-yMEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yMEd" })(new Date()) });
            map.add('sk-yMMM', { type: 'date', example: globalize.dateFormatter({ skeleton: "yMMM" })(new Date()) });
            map.add('sk-yMMMEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yMMMEd" })(new Date()) });
            map.add('sk-yMMMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yMMMd" })(new Date()) });
            map.add('sk-yMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yMd" })(new Date()) });
            map.add('sk-yyyy', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyy" })(new Date()) });
            map.add('sk-yQQQ', { type: 'date', example: globalize.dateFormatter({ skeleton: "yQQQ" })(new Date()) });
            map.add('sk-yQQQQ', { type: 'date', example: globalize.dateFormatter({ skeleton: "yQQQQ" })(new Date()) });
            map.add('sk-yyyyM', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyM" })(new Date()) });
            map.add('sk-yyyyMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyMd" })(new Date()) });
            map.add('sk-yyyyMEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyMEd" })(new Date()) });
            map.add('sk-yyyyMMM', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyMMM" })(new Date()) });
            map.add('sk-yyyyMMMd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyMMMd" })(new Date()) });
            map.add('sk-yyyyMMMEd', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyMMMEd" })(new Date()) });
            map.add('sk-yyyyMMMM', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyMMMM" })(new Date()) });
            map.add('sk-yyyyQQQ', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyQQQ" })(new Date()) });
            map.add('sk-yyyyQQQQ', { type: 'date', example: globalize.dateFormatter({ skeleton: "yyyyQQQQ" })(new Date()) });

            map.add('sk-Ehm', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "Ehm" })(new Date()) });
            map.add('sk-EHm', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "EHm" })(new Date()) });
            map.add('sk-Ehms', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "Ehms" })(new Date()) });
            map.add('sk-EHms', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "EHms" })(new Date()) });
            map.add('sk-GyMMMEdhms', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "GyMMMEdhms" })(new Date()) });
            map.add('sk-MMMEdhm', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "MMMEdhm" })(new Date()) });
            map.add('sk-yMMMdhm', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "yMMMdhm" })(new Date()) });
            map.add('sk-yQQQHm', { type: 'datetime', example: globalize.dateFormatter({ skeleton: "yQQQHm" })(new Date()) });

            return map;
        }
    }
});