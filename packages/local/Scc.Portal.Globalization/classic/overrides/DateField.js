// @require SupportedDateTimeFormat.js
// @require GlobalizeJS.js

Ext.override(Ext.form.field.Date, {
    defaultFormat: 'datetime',
    format: null,
    startDay: null,
    initComponent: function () {
        var me = this;
        if (Ext.isEmpty(this.format)) {
            me.format = Scc.Portal.Globalization.SupportedDateTimeFormat.resolveDefaultFormat(me.defaultFormat);
        }

        this.callParent(arguments);
    },

    safeParse: function (value, format) {
        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        var format = this.format;
        if (globalize.isUnicodeFormat(format)) {
            return Ext.Date.parse(value, format);
        }
        else {
            return this.callParent([value, format]);
        }
    }
});