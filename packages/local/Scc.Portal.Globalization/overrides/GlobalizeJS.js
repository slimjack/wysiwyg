// @require ../cldrjs/dist/cldr.js
// @require ../cldrjs/dist/cldr/event.js
// @require ../cldrjs/dist/cldr/supplemental.js
// @require ../cldrjs/dist/cldr/unresolved.js
// @require ../globalize/dist/globalize.js
// @require ../globalize/dist/globalize/message.js
// @require ../globalize/dist/globalize/number.js
// @require ../globalize/dist/globalize/plural.js
// @require ../globalize/dist/globalize/date.js
// @require ../globalize/dist/globalize/currency.js
// @require ../globalize/dist/globalize/relative-time.js
// @require ../globalize/dist/globalize/unit.js

// @require Data.js

// @require ../cldr-data/supplemental/likelySubtags.js
// @require ../cldr-data/supplemental/numberingSystems.js
// @require ../cldr-data/supplemental/plurals.js
// @require ../cldr-data/supplemental/ordinals.js
// @require ../cldr-data/supplemental/currencyData.js
// @require ../cldr-data/supplemental/timeData.js
// @require ../cldr-data/supplemental/weekData.js

//default (en-US) main cldr data
// @require ../cldr-data/main/en-US/numbers.js
// @require ../cldr-data/main/en-US/currencies.js
// @require ../cldr-data/main/en-US/ca-gregorian.js
// @require ../cldr-data/main/en-US/timeZoneNames.js
// @require ../cldr-data/main/en-US/dateFields.js

Ext.define('Scc.Portal.Globalization.GlobalizeJS',
{
    requires: [
      'Scc.Portal.Context.PortalContext',
      'Ext.app.Util',
      'Scc.Portal.Globalization.GlobalizeDateExtensions'
    ],
    _globalize: {},
    statics: {
        getInstance: function (language) {
            language = language || PortalContext.getCurrentCulture().language;
            if (!this._globalize[language]) {
                this._globalize[language] = this._getInstance(language);
            }
            return this._globalize[language];
        },

        loadRootMessages: function (rootMessages) {
            Globalize.loadMessages({ root: rootMessages });
            this._globalize = {};
        },

        _getInstance: function (language) {
            if (!Ext.isEmpty(window.__messages__)) {
                Globalize.loadMessages(window.__messages__);
            }

            var globalize = Globalize(language);
            if (globalize.cldr.attributes.bundle === null) {
                Ext.Error.raise("Culture specific cldr is not provided");
            }

            Scc.Portal.Globalization.GlobalizeDateExtensions.apply(globalize);
            return globalize;
        }
    }
},
function () {  
    if (!Ext.isEmpty(window.__cldr__)) {
        Globalize.load(window.__cldr__);
    }
});
