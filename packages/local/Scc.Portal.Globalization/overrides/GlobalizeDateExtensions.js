Ext.define('Scc.Portal.Globalization.GlobalizeDateExtensions', {
    statics: {
        apply: function (globalize) {
            var me = this;
            Ext.apply(globalize, {
                resolveFormatter: function (format) {
                    var cldrFormat = me._toGlobalizeFormat(globalize, format);
                    return this.dateFormatter(cldrFormat);
                },
                resolveParser: function (format) {
                    var cldrFormat = me._toGlobalizeFormat(globalize, format);
                    return this.dateParser(cldrFormat);
                },
                isUnicodeFormat: function (format) {
                    return me._isDateFormat(format) || me._isDateTimeFormat(format) || me._isTimeFormat(format) || format.indexOf('sk-') === 0 || format.indexOf('raw-') === 0;
                },
                isFormatValid: function (format) {
                    try {
                        return !!me._toGlobalizeFormat(globalize, format);
                    } catch(e) {    
                        if(e.name === "ArgumentException") {
                            return false;
                        }

                        throw new Error("Error while validating format");
                    }
                },
                formatContainsDateInfo: function (format) {
                    if (format.indexOf('t-') === 0) {
                        return false;
                    }

                    if (format.indexOf('d-') === 0 || format.indexOf('dt-') === 0) {
                        return true;
                    }

                    var stripEscapeRe = /(\\.)/g;
                    var dateInfoRe = /([GyYuUrQqMLlwWdDFgEecabB])/;

                    if (format.indexOf('raw-') === 0) {
                        return dateInfoRe.test(format.slice(4, format.length).replace(stripEscapeRe, ''));
                    }

                    return dateInfoRe.test(format.split('-')[1].replace(stripEscapeRe, ''));
                },
                formatContainsTimeInfo: function (format) {
                    if (format.indexOf('d-') === 0) {
                        return false;
                    }

                    if (format.indexOf('t-') === 0 || format.indexOf('dt-') === 0) {
                        return true;
                    }

                    var stripEscapeRe = /(\\.)/g;
                    var timeInfoRe = /([hHKkjJCmsSAt])/;

                    if (format.indexOf('raw-') === 0) {
                        return timeInfoRe.test(format.slice(4, format.length).replace(stripEscapeRe, ''));
                    }

                    return timeInfoRe.test(format.split('-')[1].replace(stripEscapeRe, ''));
                }
            });
        },
        privates: {
            _toGlobalizeFormat: function (globalize, format) {
                var splittedFormat = format.split('-');
                var cldrFormat = {};
                switch (splittedFormat[0]) {
                    case 'd':
                        if (!this._isPredefinedFormatLength(splittedFormat[1])) {
                            throw { 
                                name: "ArgumentException",
                                message: "format is wrong"
                            };
                        }

                        cldrFormat['date'] = splittedFormat[1];
                        break;

                    case 'dt':
                        if (!this._isPredefinedFormatLength(splittedFormat[1])) {
                            throw { 
                                name: "ArgumentException",
                                message: "format is wrong"
                            };
                        }

                        cldrFormat['datetime'] = splittedFormat[1];
                        break;
                    case 't':
                        if (!this._isPredefinedFormatLength(splittedFormat[1])) {
                            throw { 
                                name: "ArgumentException",
                                message: "format is wrong"
                            };
                        }

                        cldrFormat['time'] = splittedFormat[1];
                        break;
                    case 'sk':
                        if(!this._testSkeleton(globalize, splittedFormat[1])){
                            throw { 
                                name: "ArgumentException",
                                message: "format is wrong"
                            };
                        }
                        cldrFormat['skeleton'] = splittedFormat[1];
                        break;
                    case 'raw':
                        var rawFormat = format.slice(4, format.length);
                        if(!this._testRaw(globalize, rawFormat)){
                            throw { 
                                name: "ArgumentException",
                                message: "format is wrong"
                            };
                        }
                        cldrFormat['raw'] = rawFormat;
                        break;
                    default:
                        throw { 
                            name: "ArgumentException",
                            message: "format is wrong"
                        };
                }

                return cldrFormat;
            },
            _testSkeleton: function (globalize, format) {
                return this._testGlobalizeFormat(globalize, {
                    'skeleton': format
                });
            },
            _testRaw: function (globalize, format) {
                return this._testGlobalizeFormat(globalize, {
                    'raw': format
                });
            },
            _testGlobalizeFormat: function (globalize, globalizeFormat) {
                try {
                    var date = globalize.dateFormatter(globalizeFormat)(new Date());
                    return !!globalize.dateParser(globalizeFormat)(date);
                } catch (e) {
                    return false;
                }
            },
            _isPredefinedFormatLength: function (formatLength) {
                return formatLength === 'full' || formatLength === 'long' || formatLength === 'medium' || formatLength === 'short';
            },
            _isDateFormat: function (format) {
                return format.indexOf('d-') === 0 && this._isPredefinedFormatLength(format.slice(2, format.length));
            },
            _isDateTimeFormat: function (format) {
                return format.indexOf('dt-') === 0 && this._isPredefinedFormatLength(format.slice(3, format.length));
            },
            _isTimeFormat: function (format) {
                return format.indexOf('t-') === 0 && this._isPredefinedFormatLength(format.slice(2, format.length));
            }
        }
    }
});