var Scc = window.Scc || {};
Scc.Portal = Scc.Portal || {};
Scc.Portal.Globalization = Scc.Portal.Globalization || {};
Scc.Portal.Globalization.Data = Scc.Portal.Globalization.Data || {
    registerMessages: function (translations) {
        var me = this;
        window.__messages__ = window.__messages__
            ? me._merge(window.__messages__, translations)
            : translations;
    },

    registerCldr: function (cldr) {
        var me = this;
        window.__cldr__ = window.__cldr__
            ? me._merge(window.__cldr__, cldr)
            : cldr;
    },

    _isObject: function (item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    },

    _assign: function (objs) {
        return objs.reduce(function (r, o) {
            Object.keys(o).forEach(function (k) {
                r[k] = o[k];
            });
            return r;
        }, {});
    },

    _merge: function (target, source) {
        var me = this;
        var output = me._assign([{}, target]);
        if (me._isObject(target) && me._isObject(source)) {
            Object.keys(source).forEach(function (key) {
                if (me._isObject(source[key])) {
                    if (!(key in target)) {
                        var obj = {};
                        obj[key] = source[key];
                        output = me._assign([output, obj]);
                    } else {
                        output[key] = me._merge(target[key], source[key]);
                    }
                } else {
                    var obj = {};
                    obj[key] = source[key];
                    output = me._assign([output, obj]);
                }
            });
        }
        return output;
    }
};