Ext.define('Scc.Portal.Globalization.Direction', {
    override: 'Ext.Component',
    requires: [
        'Scc.Portal.Context.PortalContext',
        'Ext.rtl.*'
    ],
    rtl: PortalContext.getCurrentCulture().textDirection === 'rtl'
});