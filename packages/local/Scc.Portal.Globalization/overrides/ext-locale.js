// @require ExtJSTexts.js

Ext.onReady(function () {

    if (Ext.data && Ext.data.Types) {
        Ext.data.Types.stripRe = /[\$,%]/g;
    }

    if (Ext.Date) {
        Ext.Date.monthNames = [ExtJSTexts.t('January'), ExtJSTexts.t('February'), ExtJSTexts.t('March'), ExtJSTexts.t('April'), ExtJSTexts.t('May'), ExtJSTexts.t('June'), ExtJSTexts.t('July'), ExtJSTexts.t('August'), ExtJSTexts.t('September'), ExtJSTexts.t('October'), ExtJSTexts.t('November'), ExtJSTexts.t('December')];

        Ext.Date.shortMonthNames = [ExtJSTexts.t('shortMonthNames/Jan'), ExtJSTexts.t('shortMonthNames/Feb'), ExtJSTexts.t('shortMonthNames/Mar'), ExtJSTexts.t('shortMonthNames/Apr'), ExtJSTexts.t('shortMonthNames/May'), ExtJSTexts.t('shortMonthNames/Jun'), ExtJSTexts.t('shortMonthNames/Jul'), ExtJSTexts.t('shortMonthNames/Aug'), ExtJSTexts.t('shortMonthNames/Sep'), ExtJSTexts.t('shortMonthNames/Oct'), ExtJSTexts.t('shortMonthNames/Nov'), ExtJSTexts.t('shortMonthNames/Dec')];
        
        Ext.Date.getShortMonthName = function (month) {
            return Ext.Date.shortMonthNames[month];
        };

        Ext.Date.monthNumbers = {};
        
        Ext.Array.forEach(Ext.Date.monthNames, function(monthName, index){
            Ext.Date.monthNumbers[monthName] = index;
        });

        Ext.Array.forEach(Ext.Date.shortMonthNames, function(monthName, index){
            Ext.Date.monthNumbers[monthName] = index;
        });

        Ext.Date.getMonthNumber = function (name) {
            return Ext.Date.monthNumbers[Ext.util.Format.capitalize(name.toLowerCase())];
        };

        Ext.Date.dayNames = [ExtJSTexts.t('Sunday'), ExtJSTexts.t('Monday'), ExtJSTexts.t('Tuesday'), ExtJSTexts.t('Wednesday'), ExtJSTexts.t('Thursday'), ExtJSTexts.t('Friday'), ExtJSTexts.t('Saturday')];

        Ext.Date.shortDayNames = [ExtJSTexts.t('Sun'), ExtJSTexts.t('Mon'), ExtJSTexts.t('Tue'), ExtJSTexts.t('Wed'), ExtJSTexts.t('Thu'), ExtJSTexts.t('Fri'), ExtJSTexts.t('Sat')];

        Ext.Date.dayNumbers = {
            'sun': 0,
            'mon': 1,
            'tue': 2,
            'wed': 3,
            'thu': 4,
            'fri': 5,
            'sat': 6
        };

        Ext.Date.getShortDayName = function (day) {
            return Ext.Date.shortDayNames[day];
        };

        Ext.Date.parseCodes.S.s = "(?:st|nd|rd|th)";

        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        var firstDayOfWeekName = globalize.cldr.supplemental.weekData.firstDay();

        Ext.Date.firstDayOfWeek = Ext.Date.dayNumbers[firstDayOfWeekName];
        
        var weekendStart = globalize.cldr.get("supplemental/weekData/weekendStart/{territory}") || globalize.cldr.get("supplemental/weekData/weekendStart/001");
        var weekendEnd = globalize.cldr.get("supplemental/weekData/weekendEnd/{territory}") || globalize.cldr.get("supplemental/weekData/weekendEnd/001");

        Ext.Date.weekendDays = [Ext.Date.dayNumbers[weekendStart], Ext.Date.dayNumbers[weekendEnd]];
    }

    if (Ext.util && Ext.util.Format) {
        Ext.apply(Ext.util.Format, {
            thousandSeparator: ',',
            decimalSeparator: '.',
            currencySign: '$',
            dateFormat: 'd-short'
        });
    }
});

Ext.define("Ext.locale.en.data.validator.Bound", {
    override: "Ext.data.validator.Bound",
    emptyMessage: ExtJSTexts.t('data.validator.Bound.emptyMessage'),
    config: {
        emptyMessage: ExtJSTexts.t('data.validator.Bound.emptyMessage')
    }
});

Ext.define("Ext.locale.en.data.validator.Email", {
    override: "Ext.data.validator.Email",
    message: ExtJSTexts.t('data.validator.Email.message'),
    config: {
        message: ExtJSTexts.t('data.validator.Email.message')
    }
});

Ext.define("Ext.locale.en.data.validator.Exclusion", {
    override: "Ext.data.validator.Exclusion",
    message: ExtJSTexts.t('data.validator.Exclusion.message'),
    config: {
        message: ExtJSTexts.t('data.validator.Exclusion.message')
    }
});

Ext.define("Ext.locale.en.data.validator.Format", {
    override: "Ext.data.validator.Format",
    message: ExtJSTexts.t('data.validator.Format.message'),
    config: {
        message: ExtJSTexts.t('data.validator.Format.message')
    }
});

Ext.define("Ext.locale.en.data.validator.Inclusion", {
    override: "Ext.data.validator.Inclusion",
    message: ExtJSTexts.t('data.validator.Inclusion.message'),
    config: {
        message: ExtJSTexts.t('data.validator.Inclusion.message')
    }
});

Ext.define("Ext.locale.en.data.validator.Length", {
    override: "Ext.data.validator.Length",
    minOnlyMessage: ExtJSTexts.t('data.validator.Length.minOnlyMessage', '{0}'),
    maxOnlyMessage: ExtJSTexts.t('data.validator.Length.maxOnlyMessage', '{0}'),
    bothMessage: ExtJSTexts.t('data.validator.Length.bothMessage', '{0}', '{1}'),
    config: {
        minOnlyMessage: ExtJSTexts.t('data.validator.Length.minOnlyMessage', '{0}'),
        maxOnlyMessage: ExtJSTexts.t('data.validator.Length.maxOnlyMessage', '{0}'),
        bothMessage: ExtJSTexts.t('data.validator.Length.bothMessage', '{0}', '{1}')
    }
});

Ext.define("Ext.locale.en.data.validator.Presence", {
    override: "Ext.data.validator.Presence",
    message: ExtJSTexts.t('data.validator.Presence.message'),
    config: {
        message: ExtJSTexts.t('data.validator.Presence.message')
    }
});

Ext.define("Ext.locale.en.data.validator.Range", {
    override: "Ext.data.validator.Range",
    minOnlyMessage: ExtJSTexts.t('data.validator.Range.minOnlyMessage', '{0}'),
    maxOnlyMessage: ExtJSTexts.t('data.validator.Range.maxOnlyMessage', '{0}'),
    bothMessage: ExtJSTexts.t('data.validator.Range.bothMessage', '{0}', '{1}'),
    nanMessage: ExtJSTexts.t('data.validator.Range.nanMessage'),
    config: {
        minOnlyMessage: ExtJSTexts.t('data.validator.Range.minOnlyMessage', '{0}'),
        maxOnlyMessage: ExtJSTexts.t('data.validator.Range.maxOnlyMessage', '{0}'),
        bothMessage: ExtJSTexts.t('data.validator.Range.bothMessage', '{0}', '{1}'),
        nanMessage: ExtJSTexts.t('data.validator.Range.nanMessage')
    }
});

Ext.define("Ext.locale.en.view.View", {
    override: "Ext.view.View",
    emptyText: ""
});

Ext.define("Ext.locale.en.grid.plugin.DragDrop", {
    override: "Ext.grid.plugin.DragDrop",
    dragText: ExtJSTexts.t('grid.plugin.DragDrop.dragText', '{0}', '{1}')
});

// changing the msg text below will affect the LoadMask
Ext.define("Ext.locale.en.view.AbstractView", {
    override: "Ext.view.AbstractView",
    loadingText: ExtJSTexts.t('view.AbstractView.loadingText')
});

Ext.define("Ext.locale.en.picker.Date", {
    override: "Ext.picker.Date",
    todayText: ExtJSTexts.t('picker.Date.todayText'),
    minText: ExtJSTexts.t('picker.Date.minText'),
    maxText: ExtJSTexts.t('picker.Date.maxText'),
    disabledDaysText: "",
    disabledDatesText: "",
    nextText: ExtJSTexts.t('picker.Date.nextText'),
    prevText: ExtJSTexts.t('picker.Date.prevText'),
    monthYearText: ExtJSTexts.t('picker.Date.monthYearText'),
    todayTip: ExtJSTexts.t('picker.Date.todayTip', '{0}'),
    format: "d-short",
    startDay: null
});

Ext.define("Ext.locale.en.picker.Month", {
    override: "Ext.picker.Month",
    okText: ExtJSTexts.t('picker.Month.okText'),
    cancelText: ExtJSTexts.t('picker.Month.cancelText')
});

Ext.define("Ext.locale.en.toolbar.Paging", {
    override: "Ext.PagingToolbar",
    beforePageText: ExtJSTexts.t('toolbar.Paging.beforePageText'),
    afterPageText: ExtJSTexts.t('toolbar.Paging.afterPageText', '{0}'),
    firstText: ExtJSTexts.t('toolbar.Paging.firstText'),
    prevText: ExtJSTexts.t('toolbar.Paging.prevText'),
    nextText: ExtJSTexts.t('toolbar.Paging.nextText'),
    lastText: ExtJSTexts.t('toolbar.Paging.lastText'),
    refreshText: ExtJSTexts.t('toolbar.Paging.refreshText'),
    displayMsg: ExtJSTexts.t('toolbar.Paging.displayMsg', '{0}', '{1}', '{2}'),
    emptyMsg: ExtJSTexts.t('toolbar.Paging.emptyMsg')
});

Ext.define("Ext.locale.en.form.Basic", {
    override: "Ext.form.Basic",
    waitTitle: ExtJSTexts.t('form.Basic.waitTitle')
});

Ext.define("Ext.locale.en.form.field.Base", {
    override: "Ext.form.field.Base",
    invalidText: ExtJSTexts.t('form.field.Base.invalidText')
});

Ext.define("Ext.locale.en.form.field.Text", {
    override: "Ext.form.field.Text",
    minLengthText: ExtJSTexts.t('form.field.Text.minLengthText', '{0}'),
    maxLengthText: ExtJSTexts.t('form.field.Text.maxLengthText', '{0}'),
    blankText: ExtJSTexts.t('form.field.Text.blankText'),
    regexText: "",
    emptyText: null
});

Ext.define("Ext.locale.en.form.field.Number", {
    override: "Ext.form.field.Number",
    decimalPrecision: 2,
    minText: ExtJSTexts.t('form.field.Number.minText', '{0}'),
    maxText: ExtJSTexts.t('form.field.Number.maxText', '{0}'),
    nanText: ExtJSTexts.t('form.field.Number.nanText', '{0}')
});

Ext.define("Ext.locale.en.form.field.Date", {
    override: "Ext.form.field.Date",
    disabledDaysText: ExtJSTexts.t('form.field.Date.disabledDaysText'),
    ariaDisabledDaysText: ExtJSTexts.t('form.field.Date.ariaDisabledDaysText'),
    ariaDisabledDatesText: ExtJSTexts.t('form.field.Date.ariaDisabledDatesText'),
    disabledDatesText: ExtJSTexts.t('form.field.Date.disabledDatesText'),
    minText: ExtJSTexts.t('form.field.Date.minText', '{0}'),
    ariaMinText: ExtJSTexts.t('form.field.Date.ariaMinText', '{0}'),
    maxText: ExtJSTexts.t('form.field.Date.maxText', '{0}'),
    ariaMaxText: ExtJSTexts.t('form.field.Date.ariaMaxText', '{0}'),
    invalidText: ExtJSTexts.t('form.field.Date.invalidText', '{0}', '{1}'),
    format: "d-short",
    formatText: ExtJSTexts.t('form.field.Date.formatText', '{0}'),
    altFormats: "d-medium|d-long|d-full"
});

Ext.define("Ext.locale.en.form.field.ComboBox", {
    override: "Ext.form.field.ComboBox",
    valueNotFoundText: undefined
}, function () {
    Ext.apply(Ext.form.field.ComboBox.prototype.defaultListConfig, {
        loadingText: ExtJSTexts.t('form.field.ComboBox.loadingText')
    });
});

Ext.define("Ext.locale.en.form.field.VTypes", {
    override: "Ext.form.field.VTypes",
    emailText: ExtJSTexts.t('form.field.VTypes.emailText'),
    urlText: ExtJSTexts.t('form.field.VTypes.urlText'),
    alphaText: ExtJSTexts.t('form.field.VTypes.alphaText'),
    alphanumText: ExtJSTexts.t('form.field.VTypes.alphanumText')
});

Ext.define("Ext.locale.en.form.field.HtmlEditor", {
    override: "Ext.form.field.HtmlEditor",
    createLinkText: 'Please enter the URL for the link:'
}, function () {
    Ext.apply(Ext.form.field.HtmlEditor.prototype, {
        buttonTips: {
            bold: {
                title: ExtJSTexts.t('form.field.HtmlEditor.bold.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.bold.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            italic: {
                title: ExtJSTexts.t('form.field.HtmlEditor.italic.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.italic.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            underline: {
                title: ExtJSTexts.t('form.field.HtmlEditor.underline.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.underline.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            increasefontsize: {
                title: ExtJSTexts.t('form.field.HtmlEditor.increasefontsize.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.increasefontsize.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            decreasefontsize: {
                title: ExtJSTexts.t('form.field.HtmlEditor.decreasefontsize.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.decreasefontsize.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            backcolor: {
                title: ExtJSTexts.t('form.field.HtmlEditor.backcolor.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.backcolor.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            forecolor: {
                title: ExtJSTexts.t('form.field.HtmlEditor.forecolor.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.forecolor.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyleft: {
                title: ExtJSTexts.t('form.field.HtmlEditor.justifyleft.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.justifyleft.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifycenter: {
                title: ExtJSTexts.t('form.field.HtmlEditor.justifycenter.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.justifycenter.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyright: {
                title: ExtJSTexts.t('form.field.HtmlEditor.justifyright.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.justifyright.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertunorderedlist: {
                title: ExtJSTexts.t('form.field.HtmlEditor.insertunorderedlist.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.insertunorderedlist.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertorderedlist: {
                title: ExtJSTexts.t('form.field.HtmlEditor.insertorderedlist.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.insertorderedlist.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            createlink: {
                title: ExtJSTexts.t('form.field.HtmlEditor.createlink.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.createlink.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            sourceedit: {
                title: ExtJSTexts.t('form.field.HtmlEditor.sourceedit.title'),
                text: ExtJSTexts.t('form.field.HtmlEditor.sourceedit.text'),
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            }
        }
    });
});

Ext.define("Ext.locale.en.grid.header.Container", {
    override: "Ext.grid.header.Container",
    sortAscText: ExtJSTexts.t('grid.header.Container.sortAscText'),
    sortDescText: ExtJSTexts.t('grid.header.Container.sortDescText'),
    columnsText: ExtJSTexts.t('grid.header.Container.columnsText')
});

Ext.define("Ext.locale.en.grid.GroupingFeature", {
    override: "Ext.grid.feature.Grouping",
    emptyGroupText: ExtJSTexts.t('grid.GroupingFeature.emptyGroupText'),
    groupByText: ExtJSTexts.t('grid.GroupingFeature.groupByText'),
    showGroupsText: ExtJSTexts.t('grid.GroupingFeature.showGroupsText')
});

Ext.define("Ext.locale.en.grid.PropertyColumnModel", {
    override: "Ext.grid.PropertyColumnModel",
    nameText: ExtJSTexts.t('grid.PropertyColumnModel.nameText'),
    valueText: ExtJSTexts.t('grid.PropertyColumnModel.valueText'),
    dateFormat: "d-short",
    trueText: ExtJSTexts.t('grid.PropertyColumnModel.trueText'),
    falseText: ExtJSTexts.t('grid.PropertyColumnModel.falseText')
});

Ext.define("Ext.locale.en.grid.BooleanColumn", {
    override: "Ext.grid.BooleanColumn",
    trueText: ExtJSTexts.t('grid.BooleanColumn.trueText'),
    falseText: ExtJSTexts.t('grid.BooleanColumn.falseText'),
    undefinedText: '&#160;'
});

Ext.define("Ext.locale.en.grid.NumberColumn", {
    override: "Ext.grid.NumberColumn",
    format: '0,000.00'
});

Ext.define("Ext.locale.en.grid.DateColumn", {
    override: "Ext.grid.DateColumn",
    format: 'd-short'
});

Ext.define("Ext.locale.en.form.field.Time", {
    override: "Ext.form.field.Time",
    minText: ExtJSTexts.t('form.field.Time.minText', '{0}'),
    maxText: ExtJSTexts.t('form.field.Time.maxText', '{0}'),
    invalidText: ExtJSTexts.t('form.field.Time.invalidText', '{0}'),
    format: "t-short",
    altFormats: "t-medium|t-long|t-full"
});

Ext.define("Ext.locale.en.form.field.File", {
    override: "Ext.form.field.File",
    buttonText: ExtJSTexts.t('form.field.File.buttonText')
});

Ext.define("Ext.locale.en.form.CheckboxGroup", {
    override: "Ext.form.CheckboxGroup",
    blankText: ExtJSTexts.t('form.CheckboxGroup.blankText')
});

Ext.define("Ext.locale.en.form.RadioGroup", {
    override: "Ext.form.RadioGroup",
    blankText: ExtJSTexts.t('form.RadioGroup.blankText')
});

Ext.define("Ext.locale.en.window.MessageBox", {
    override: "Ext.window.MessageBox",
    buttonText: {
        ok: ExtJSTexts.t('window.MessageBox.buttonText.ok'),
        cancel: ExtJSTexts.t('window.MessageBox.buttonText.cancel'),
        yes: ExtJSTexts.t('window.MessageBox.buttonText.yes'),
        no: ExtJSTexts.t('window.MessageBox.buttonText.no')
    }
});

Ext.define("Ext.locale.en.grid.filters.Filters", {
    override: "Ext.grid.filters.Filters",
    menuFilterText: ExtJSTexts.t('grid.filters.Filters.menuFilterText')
});

Ext.define("Ext.locale.en.grid.filters.filter.Boolean", {
    override: "Ext.grid.filters.filter.Boolean",
    yesText: ExtJSTexts.t('grid.filters.filter.Boolean.yesText'),
    noText: ExtJSTexts.t('grid.filters.filter.Boolean.noText')
});

Ext.define("Ext.locale.en.grid.filters.filter.Date", {
    override: "Ext.grid.filters.filter.Date",
    fields: {
        lt: { text: ExtJSTexts.t('grid.filters.filter.Date.fields.lt.text') },
        gt: { text: ExtJSTexts.t('grid.filters.filter.Date.fields.gt.text') },
        eq: { text: ExtJSTexts.t('grid.filters.filter.Date.fields.eq.text') }
    },
    dateFormat: "d-short"
});

Ext.define("Ext.locale.en.grid.filters.filter.List", {
    override: "Ext.grid.filters.filter.List",
    loadingText: ExtJSTexts.t('grid.filters.filter.List.loadingText')
});

Ext.define("Ext.locale.en.grid.filters.filter.Number", {
    override: "Ext.grid.filters.filter.Number",
    emptyText: ExtJSTexts.t('grid.filters.filter.Number.emptyText')
});

Ext.define("Ext.locale.en.grid.filters.filter.String", {
    override: "Ext.grid.filters.filter.String",
    emptyText: ExtJSTexts.t('grid.filters.filter.String.emptyText')
});

Ext.define("Ext.locale.en.view.MultiSelectorSearch", {
    override: 'Ext.view.MultiSelectorSearch',
    searchText: ExtJSTexts.t('view.MultiSelectorSearch.searchText')
});

Ext.define("Ext.locale.en.view.MultiSelector", {
    override: 'Ext.view.MultiSelector',
    emptyText: ExtJSTexts.t('view.MultiSelector.emptyText'),
    removeRowTip: ExtJSTexts.t('view.MultiSelector.removeRowTip'),
    addToolText: ExtJSTexts.t('view.MultiSelector.addToolText')
});

Ext.define("Ext.locale.en.panel.Panel", {
    override: 'Ext.panel.Panel',
    closeToolText: ExtJSTexts.t('panel.Panel.closeToolText'),
    collapseToolText: ExtJSTexts.t('panel.Panel.collapseToolText'),
    expandToolText: ExtJSTexts.t('panel.Panel.expandToolText')
});

Ext.define("Ext.locale.en.window.Window", {
    override: 'Ext.window.Window',
    closeToolText: ExtJSTexts.t('window.Window.closeToolText')
});

Ext.define("Ext.locale.en.grid.feature.Grouping", {
    override: 'Ext.grid.feature.Grouping',
    groupByText: ExtJSTexts.t('grid.feature.Grouping.groupByText'),
    showGroupsText: ExtJSTexts.t('grid.feature.Grouping.showGroupsText'),
    expandTip: ExtJSTexts.t('grid.feature.Grouping.expandTip'),
    collapseTip: ExtJSTexts.t('grid.feature.Grouping.collapseTip')
});

Ext.define("Ext.locale.en.grid.RowEditor", {
    override: 'Ext.grid.RowEditor',
    saveBtnText: ExtJSTexts.t('grid.RowEditor.saveBtnText'),
    cancelBtnText: ExtJSTexts.t('grid.RowEditor.cancelBtnText'),
    errorsText: ExtJSTexts.t('grid.RowEditor.errorsText'),
    dirtyText: ExtJSTexts.t('grid.RowEditor.dirtyText')
});

Ext.define("Ext.locale.en.ux.LiveSearchGridPanel", {
    override: 'Ext.ux.LiveSearchGridPanel',
    defaultStatusText: ExtJSTexts.t('ux.LiveSearchGridPanel.defaultStatusText'),
    defaultSearchText: ExtJSTexts.t('ux.LiveSearchGridPanel.defaultSearchText'),
    defaultFindPreviousRowToolTipText: ExtJSTexts.t('ux.LiveSearchGridPanel.defaultFindPreviousRowToolTipText'),
    defaultFindNextRowToolTipText: ExtJSTexts.t('ux.LiveSearchGridPanel.defaultFindNextRowToolTipText'),
    defaultRegularExpressionText: ExtJSTexts.t('ux.LiveSearchGridPanel.defaultRegularExpressionText'),
    defaultCaseSensitiveText: ExtJSTexts.t('ux.LiveSearchGridPanel.defaultCaseSensitiveText')
});

// This is needed until we can refactor all of the locales into individual files
Ext.define("Ext.locale.en.Component", {
    override: "Ext.Component"
});

Ext.define("Ext.locale.en.LoadMask", {
    override: "Ext.LoadMask",
    msg: ExtJSTexts.t('view.AbstractView.loadingText')
});

Ext.define("Ext.locale.en.dashboard.Panel", {
    override: "Ext.dashboard.Panel",
    msg: ExtJSTexts.t('view.AbstractView.loadingText')
});

Ext.define("Ext.locale.en.util.StoreHolder", {
    override: "Ext.util.StoreHolder",
    msg: ExtJSTexts.t('view.AbstractView.loadingText')
});