// @require GlobalizeJS.js

Ext.define('Scc.Portal.Globalization.SupportedDateTimeFormat', {
    alternateClassName: 'SupportedDateTimeFormat',
    statics: {
        isValid: function (format) {
            if (!format) {
                return false;
            }

            var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
            if (globalize.isUnicodeFormat(format)) {
                return globalize.isFormatValid(format);
            }

            return Ext.Date.format(new Date(), format) !== format;
        },
        getDateDefaultFormat: function () {
            var htmlTag = Ext.fly(document.documentElement);
            return htmlTag.getAttribute('data-default-date-format') || 'd-short';
        },
        getDateTimeDefaultFormat: function () {
            var htmlTag = Ext.fly(document.documentElement);
            return htmlTag.getAttribute('data-default-date-time-format') || 'dt-short';
        },
        getTimeDefaultFormat: function () {
            var htmlTag = Ext.fly(document.documentElement);
            return htmlTag.getAttribute('data-default-time-format') || 't-short';
        },
        resolveDefaultFormat: function (defaultFormat) {
            switch (defaultFormat) {
                case 'date':
                    return this.getDateDefaultFormat();
                case 'datetime':
                    return this.getDateTimeDefaultFormat();
                case 'time':
                    return this.getTimeDefaultFormat();
                default:
                    Ext.Error.raise('DefaultFormat is wrong');
            }
        }
    }
});