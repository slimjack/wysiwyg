// @require GlobalizeJS.js

Ext.define('Scc.Portal.Globalization.Texts', {
    requires: [
        'Scc.Portal.Globalization.GlobalizeJS'
    ],
    constructor: function () {
        this.callParent(arguments);
        if (!this.root) {
            Ext.Error.raise("Root messages shall be provided");
        }
        var namespace = this._getMessageNamespace(Ext.getClassName(this));
        this._keyPrefix = namespace + '/';
        var rootMessages = {};
        rootMessages[namespace] = this.root;
        Scc.Portal.Globalization.GlobalizeJS.loadRootMessages(rootMessages);
    },

    t: function (key) {
        var args = Ext.Array.slice(arguments, 1);
        return Scc.Portal.Globalization.GlobalizeJS.getInstance().messageFormatter(this._keyPrefix + key).apply(null, args);
    },

    privates: {
        _getMessageNamespace: function (className) {
            var pos = className.lastIndexOf(".");
            if (pos === -1) {
                return 'global';
            }
            return className.substring(0, pos);
        }
    }
});
