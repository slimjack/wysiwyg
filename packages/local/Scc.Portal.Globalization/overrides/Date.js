// @require GlobalizeJS.js

Ext.override(Ext.Date, {
    originalCreateFormatFunction: Ext.Date.createFormat,
    originalCreateParserFunction: Ext.Date.createParser,
    originalFormatContainsHourInfo: Ext.Date.formatContainsHourInfo,
    originalFormatContainsDateInfo: Ext.Date.formatContainsDateInfo,

    createFormat: function (format) {
        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        if (globalize.isUnicodeFormat(format)) {
            var formatter = globalize.resolveFormatter(format);
            Ext.Date.formatFunctions[format] = function () {
                return formatter(this);
            };
        }
        else {
            this.originalCreateFormatFunction(format);
        }
    },
    createParser: function (format) {
        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        if (globalize.isUnicodeFormat(format)) {
            var parser = globalize.resolveParser(format);
            Ext.Date.parseFunctions[format] = function (input) {
                return parser(input);
            };
        }
        else {
            this.originalCreateParserFunction(format);
        }
    },
    formatContainsHourInfo: function (format) {
        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        if (globalize.isUnicodeFormat(format)) {
            return globalize.formatContainsTimeInfo(format);
        }
        else {
            return this.originalFormatContainsHourInfo(format);
        }
    },
    formatContainsDateInfo: function (format) {
        var globalize = Scc.Portal.Globalization.GlobalizeJS.getInstance();
        if (globalize.isUnicodeFormat(format)) {
            return globalize.formatContainsDateInfo(format);
        }
        else {
            return this.originalFormatContainsDateInfo(format);
        }
    }
});