Scc.Portal.Globalization.Data.registerCldr({
  "main": {
    "uk": {
      "identity": {
        "version": {
          "_number": "$Revision: 13712 $",
          "_cldrVersion": "32"
        },
        "language": "uk"
      },
      "layout": {
        "orientation": {
          "characterOrder": "left-to-right",
          "lineOrder": "top-to-bottom"
        }
      }
    }
  }
}
);