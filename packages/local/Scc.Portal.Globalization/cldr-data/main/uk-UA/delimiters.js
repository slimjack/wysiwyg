Scc.Portal.Globalization.Data.registerCldr({
  "main": {
    "uk": {
      "identity": {
        "version": {
          "_number": "$Revision: 13712 $",
          "_cldrVersion": "32"
        },
        "language": "uk"
      },
      "delimiters": {
        "quotationStart": "«",
        "quotationEnd": "»",
        "alternateQuotationStart": "„",
        "alternateQuotationEnd": "“"
      }
    }
  }
}
);