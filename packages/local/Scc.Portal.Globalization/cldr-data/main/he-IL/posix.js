Scc.Portal.Globalization.Data.registerCldr({
  "main": {
    "he": {
      "identity": {
        "version": {
          "_number": "$Revision: 13722 $",
          "_cldrVersion": "32"
        },
        "language": "he"
      },
      "posix": {
        "messages": {
          "yesstr": "כן:כ",
          "nostr": "לא:ל"
        }
      }
    }
  }
}
);