Scc.Portal.Globalization.Data.registerCldr({
  "main": {
    "he": {
      "identity": {
        "version": {
          "_number": "$Revision: 13722 $",
          "_cldrVersion": "32"
        },
        "language": "he"
      },
      "delimiters": {
        "quotationStart": "”",
        "quotationEnd": "”",
        "alternateQuotationStart": "’",
        "alternateQuotationEnd": "’"
      }
    }
  }
}
);