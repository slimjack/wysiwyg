Scc.Portal.Globalization.Data.registerCldr({
  "main": {
    "he": {
      "identity": {
        "version": {
          "_number": "$Revision: 13722 $",
          "_cldrVersion": "32"
        },
        "language": "he"
      },
      "listPatterns": {
        "listPattern-type-standard": {
          "start": "{0}, {1}",
          "middle": "{0}, {1}",
          "end": "{0} ו{1}",
          "2": "{0} ו{1}"
        },
        "listPattern-type-or": {
          "start": "{0}, {1}",
          "middle": "{0}, {1}",
          "end": "{0}, או {1}",
          "2": "{0} או {1}"
        },
        "listPattern-type-standard-short": {
          "start": "{0}, {1}",
          "middle": "{0}, {1}",
          "end": "{0} ו{1}",
          "2": "{0} ו{1}"
        },
        "listPattern-type-unit": {
          "start": "{0}, {1}",
          "middle": "{0}, {1}",
          "end": "{0} ו{1}",
          "2": "{0}, {1}"
        },
        "listPattern-type-unit-narrow": {
          "start": "{0} {1}",
          "middle": "{0} {1}",
          "end": "{0} {1}",
          "2": "{0} {1}"
        },
        "listPattern-type-unit-short": {
          "start": "{0}, {1}",
          "middle": "{0}, {1}",
          "end": "{0}, {1}",
          "2": "{0}, {1}"
        }
      }
    }
  }
}
);