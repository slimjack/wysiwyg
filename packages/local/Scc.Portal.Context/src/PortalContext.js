Ext.define('Scc.Portal.Context.PortalContext', {
    alternateClassName: 'PortalContext',
    requires:['Ext.dom.Fly'],
    statics: {
        getCurrentCulture: function() {
            if (!this._currentCulture) {
                this._currentCulture = this._getCurrentCulture();
            }
            return this._currentCulture;
        },

        privates: {
            _defaultLanguage: 'en-US',
            _defaultDirection: 'ltr',

            _getCurrentCulture: function () {
                var htmlTag = Ext.fly(document.documentElement);
                var lang = htmlTag.getAttribute('lang');
                var dir = htmlTag.getAttribute('dir');
                if (!lang) {
                    lang = this._defaultLanguage;
                    dir = this._defaultDirection;
                    console.warn('The attribute "lang" not specified. Default "' + this._defaultLanguage + '" language is used');
                } else if (!dir) {
                    dir = this._defaultDirection;
                    console.warn('The attribute "dir" not specified. Default "' + this._defaultDirection + '" text direction is used');
                }

                return Object.freeze({
                    language: lang,
                    textDirection: dir
                });
            }
        }
    }
});