Ext.define('WYSIWYG.view.ViewPort', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.demoviewport',
    layout: 'fit',

    requires: [
        'Ext.layout.container.Fit',
        'WYSIWYG.view.Entry'
    ],

    items: [{
        xtype: 'entry'
    }]
});
