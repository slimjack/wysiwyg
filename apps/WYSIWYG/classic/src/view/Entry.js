Ext.define('WYSIWYG.view.Entry', {
    alias: 'widget.entry',
    extend: 'Ext.panel.Panel',
    itemId: 'main',
    requires: [
        'Ext.layout.container.HBox',
        'Ext.layout.container.VBox',
        'Scc.Portal.CustomizableForms.Container',
        'Scc.Portal.CustomizableForms.ToolboxItem',
        'Ext.Img',
        'Ext.button.Button'
    ],
    title: 'Consent form editor',
    layout: 'hbox',
    defaults: {
        margin: 2
    },
    items: [{
        xtype: 'panel',
        layout: 'vbox',
        title: 'Toolbox',
        border: true,
        defaults: {
            margin: 2
        },
        items: [{
            xtype: 'button',
            text: "Button",
            width: 100,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'button',
                    text: "Button",
                    margin: 2,
                    width: 100
                }
            }]
        }, {
            xtype: 'textfield',
            fieldLabel: 'Text Field',
            width: 200,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'textfield',
                    fieldLabel: "Text Field",
                    margin: 2,
                    minWidth: 100
                }
            }]
        }, {
            xtype: 'label',
            text: 'Label',
            width: 200,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'label',
                    text: "Signature:",
                    margin: 2
                }
            }]
        }, {
            xtype: 'container',
            layout: 'fit',
            html: '<div style="text-align: center;">H-container</div>',
            width: 200,
            style: "border: 1px dashed black",
            height: 50,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'container',
                    plugins: ['customizable.container'],
                    layout: 'hbox',
                    minWidth: 50,
                    minHeight: 50
                }
            }]
        }, {
            xtype: 'container',
            layout: 'fit',
            html: '<div style="text-align: center;">V-container</div>',
            width: 200,
            style: "border: 1px dashed black",
            height: 50,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'container',
                    plugins: ['customizable.container'],
                    layout: 'vbox',
                    minWidth: 50,
                    minHeight: 50
                }
            }]
        }, {
            xtype: 'container',
            layout: 'fit',
            html: '<div style="text-align: center;">Text container</div>',
            width: 200,
            style: "border: 1px dashed black",
            height: 50,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'component',
                    html: "Prior to receiving this treatment, I have been candid in revealing any condition that may have bearing on this procedure, such as pregnancy"
                    + " (if so, please consult your physician prior to treatment), recent surgery, allergies, tendency to cold sores/fever blisters, and use of any/all"
                    + " topical/oral prescription medications.<br/>I understand that side effects and complications are usually minimal, and I understand that I may experience discomfort.",
                    minWidth: 50,
                    minHeight: 50
                }
            }]
        }, {
            xtype: 'container',
            layout: 'fit',
            html: '<div style="text-align: center;">Signature field</div>',
            width: 200,
            style: "border: 1px dashed black",
            height: 50,
            plugins: [{
                ptype: 'customizable.toolboxitem',
                itemConfig: {
                    xtype: 'container',
                    style: "border: 2px solid gray",
                    minWidth: 50,
                    minHeight: 50
                }
            }]
        }]
    }, {
        xtype: 'container',
        minWidth: 100,
        items: [{
            xtype: 'button',
            text: "Set Designer Mode",
            width: 150,
            handler: function () {
                this.up('entry').down('[itemId="maindroppanel"]').setDesignerMode(true);
            }
        }, {
            xtype: 'button',
            text: "Reset Designer Mode",
            width: 150,
            handler: function () {
                this.up('entry').down('[itemId="maindroppanel"]').setDesignerMode(false);
            }
       }]
    }, {
        xtype: 'panel',
        title: 'Layout',
        border: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        plugins: ['customizable.container'],
        itemId: 'maindroppanel',
        minWidth: 100,
        minHeight: 100,
        width: 400,
        defaults: {
            margin: 2
        },
        items: [{
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                margin: 2
            },
            items: [{
                xtype: 'image',
                src: "logo.png",
                width: 200,
                height: 20
            }, {
                xtype: 'container',
                flex: 1
            }]
        }, {
            xtype: 'container',
            html: '<div style="text-align: center; font-size: 14pt;"><strong>Patient Consent</strong></div>',
            width: 150,
            margin: '5 2 5 2'
        }, {
            xtype: 'component',
            html: "Prior to receiving this treatment, I have been candid in revealing any condition that may have bearing on this procedure, such as pregnancy"
            + " (if so, please consult your physician prior to treatment), recent surgery, allergies, tendency to cold sores/fever blisters, and use of any/all"
            + " topical/oral prescription medications.<br/>I understand that side effects and complications are usually minimal, and I understand that I may experience discomfort.",
            margin: '10 2 10 2'
        }, {
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                margin: 2
            },
            items: [{
                xtype: 'container',
                flex: 1
            }, {
                xtype: 'label',
                text: "Signature:"
            }, {
                xtype: 'container',
                style: "border: 2px solid gray",
                width: 200,
                height: 100
            }]
        }, {
            xtype: 'container',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            defaults: {
                margin: 2
            },
            items: [{
                xtype: 'button',
                text: "Submit",
                width: 100
            }]
        }]
    }]
});
