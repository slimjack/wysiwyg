/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'WYSIWYG.Application',

    name: 'WYSIWYG',

    requires: [
        // This will automatically load all classes in the EntryBaseDemo namespace
        // so that application classes do not need to require each other.
        'WYSIWYG.*'
    ],

    // The name of the initial view to create.
    mainView: 'WYSIWYG.view.ViewPort'
});